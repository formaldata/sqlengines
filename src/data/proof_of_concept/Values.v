(************************************************************************************)
(**                                                                                 *)
(**                             The SQLEngines Library                              *)
(**                                                                                 *)
(**            LRI, CNRS & Université Paris-Sud, Université Paris-Saclay            *)
(**                                                                                 *)
(**                        Copyright 2016-2019 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                                                                                 *)
(************************************************************************************)

Require Import Arith NArith ZArith String.
Require Import OrderedSet FiniteSet.

(** Types a.k.a domains in the database textbooks. *)
Inductive type := 
 | type_string 
 | type_nat 
 | type_N 
 | type_Z.

(** Embedding several coq datatypes (corresponding to domains) into a single uniform
    type for values.
*)
Inductive value : Set :=
  | Value_string : string -> value
  | Value_nat : nat -> value
  | Value_N : N -> value
  | Value_Z : Z -> value.

Definition type_of_value v := 
match v with
  | Value_string _  => type_string
  | Value_nat _ => type_nat
  | Value_N _ => type_N
  | Value_Z _ => type_Z
  end.

(** Default values for each type. *)
Definition default_value d :=
  match d with
    | type_string => Value_string EmptyString
    | type_nat => Value_nat 0
    | type_N => Value_N 0
    | type_Z => Value_Z 0
  end.

(** injection of domain names into natural numbers in order to
    build an ordering on them.
*)

Open Scope N_scope.

Definition N_of_type := 
    fun d => 
    match d with   
    | type_string => 0
    | type_nat => 1
    | type_N => 2
    | type_Z => 3
    end.

Definition OT : Oset.Rcd type.
apply Oemb with N_of_type.
intros d1 d2; case d1; case d2;
(exact (fun _ => refl_equal _) || (intro Abs; discriminate Abs)).
Defined.


(** Comparison over values, in order to build an ordered type over values, and then
    finite sets.
*)
Definition value_compare x y := 
  match x, y with
    | Value_string s1, Value_string s2 => string_compare s1 s2
    | Value_string _, _ => Lt 
    | Value_nat n1, Value_nat n2 => nat_compare n1 n2
    | Value_nat _, Value_string _ => Gt
    | Value_nat _, _ => Lt
    | Value_N n1, Value_N n2 => Ncompare n1 n2
    | Value_N _, Value_Z _ => Lt
    | Value_N _, _ => Gt
    | Value_Z z1, Value_Z z2 => Zcompare z1 z2
    | Value_Z _, _ => Gt
  end.

Definition OVal : Oset.Rcd value.
split with value_compare.
(* 1/3 *)
intros [s1 | n1 | n1 | z1] [s2 | n2 | n2 | z2]; try discriminate.
generalize (Oset.eq_bool_ok Ostring s1 s2); simpl; case (string_compare s1 s2).
apply f_equal.
intros H1 H2; injection H2; apply H1.
intros H1 H2; injection H2; apply H1.
generalize (Oset.eq_bool_ok Onat n1 n2); simpl; case (nat_compare n1 n2).
apply f_equal.
intros H1 H2; injection H2; apply H1.
intros H1 H2; injection H2; apply H1.
generalize (Oset.eq_bool_ok ON n1 n2); simpl; case (Ncompare n1 n2).
apply f_equal.
intros H1 H2; injection H2; apply H1.
intros H1 H2; injection H2; apply H1.
generalize (Oset.eq_bool_ok OZ z1 z2); simpl; case (Zcompare z1 z2).
apply f_equal.
intros H1 H2; injection H2; apply H1.
intros H1 H2; injection H2; apply H1.
(* 1/2 *)
intros [s1 | n1 | n1 | z1] [s2 | n2 | n2 | z2] [s3 | n3 | n3 | z3]; trivial; try discriminate; simpl.
apply (Oset.compare_lt_trans Ostring).
apply (Oset.compare_lt_trans Onat).
apply (Oset.compare_lt_trans ON).
apply (Oset.compare_lt_trans OZ).
(* 1/1 *)
intros [s1 | n1 | n1 | z1] [s2 | n2 | n2 | z2]; trivial; simpl.
apply (Oset.compare_lt_gt Ostring).
apply (Oset.compare_lt_gt Onat).
apply (Oset.compare_lt_gt ON).
apply (Oset.compare_lt_gt OZ).
Defined.

Definition FVal := Fset.build OVal.
