# SQLEngines

A Coq specification and formalization of the low level layer of SQL's execution engines


## Compilation
This work compiles with Coq 8.6.1:
```
./build_make
make
```


## License

This code is released under the terms of the LGPLv3 license; see LICENSE for details.


## Companion paper
[A Coq formalisation of SQL's execution engines](https://hal.archives-ouvertes.fr/hal-01716048), Benzaken, Véronique; Contejean, Évelyne; Keller, Chantal; Martins, Eunice, [ITP - 9th International Conference on Interactive Theorem Proving](https://itp2018.inria.fr) - 2018
